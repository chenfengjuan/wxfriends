/**
 * Created by chenfengjuan on 17/2/10.
 */
$().ready(function () {
    new app();
});
var app=function () {
    this.initialize();
}
app.prototype={
    initialize: function () {
        this.setLayout();
        this.setEvents();
        this.requstData(1);
    },
    initpageLabel: function (count,total,currentpage) {
        $("#totalPages").text(count);
        $("#p1").text(currentpage);
        $("#p2").text(total);
    },
    render: function (data) {
        $("#taskManagement").empty();
        this.initpageLabel(data.count,data.max_page,data.page);
        this.initPages('pageMenu',data.max_page,data.page);
        var operating="<a class='tableA lookDetails' href='#'>查看详情</a>";
        $.each(data.data,function (i, item) {
            if(item.status=="ready"){
                operating="<a class='tableA lookDetails' href='#'>查看详情</a><a class='tableA taskEdit' href='#'>编辑</a><a class='tableA taskDelete' href='#'>删除</a>";
            }else {
                operating="<a class='tableA lookDetails' href='#'>查看详情</a>";
            }
            var sta="";
            if(item.status=="finish"){
                sta="已发送";
            }else if(item.status=="ready"){
                sta="未发送";
            }else {
                sta="已发送("+item.status+")";
            }
            $("#taskManagement").append('<tr> ' +
                '<td>'+item.numid+'</td> ' +
                '<td>'+item.title+'</td> ' +
                '<td>'+item.post_time+'</td> ' +
                '<td>'+item.create_time+'</td> ' +
                '<td>'+item.last_update+'</td> ' +
                '<td>'+sta+'</td> ' +
                '<td name='+item.numid+'>'+operating+'</td> </tr>');
        });
    },
    requstData:function (page) {
        var self=this;
        var formData=new Object();
        formData.title=$("#taskTitleSearch").val();
        formData.status=$("#taskstatus").find("option:selected").val();
        if(page!=undefined){
            formData.page=page;
        }else{
            formData.page=1;
        }
        formData.size=20;
        console.log(formData);
        $.ajax({
            url: 'http://192.168.0.107:2000/pyq/task_list/',
            type: 'GET',
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                self.render(data);
            }
        });
        // var data={
        //     data:[
        //         {id:"001",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"002",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送(8/10)"},
        //         {id:"003",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"004",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送(20/20)"},
        //         {id:"005",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"006",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送(2/10)"},
        //         {id:"007",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"008",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"009",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"010",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"011",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"012",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"013",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"014",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"015",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"016",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"001",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"002",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"001",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"002",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"001",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"002",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"001",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"002",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"001",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"002",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"},
        //         {id:"001",title:"测试朋友圈一",post_time:"2017-02-01 10:00",createTime:"2017-01-31 18:00",lastTime:"2017-01-31 18:00",status:"未发送"},
        //         {id:"002",title:"测试朋友圈2",post_time:"2017-01-31 18:00",createTime:"2017-01-31 18:00",lastTime:"2017-02-1 18:00",status:"已发送"}
        //     ],
        //     page:1,
        //     max_page:20,
        //     count:2000
        // }
        // this.render(data);
    },
    setLayout: function () {
        var bodyW=$(document.body).width();
        var bodyH=$(window).height();
        var minH=parseInt($('html').css('minHeight'));
        if(bodyH<=minH){
            bodyH=minH;
        }
        var headerH=$(".headerContainer").outerHeight();
        var sideW=$(".sideContainer").outerWidth();
        $(".bodyContainer").css({'height':bodyH-headerH+'px'});
        var padding=parseInt($(".contentContainer").css("padding"))*2;
        $(".contentContainer").css({'width':bodyW-sideW-padding+'px','height':bodyH-headerH-padding+'px'});
        $("#taskManagement").css({'maxHeight': bodyH-headerH-padding-160+'px'});
        $(".taskDetailsContent").css({height:bodyH-headerH-padding-41+'px'});
        if(!$(".detailsMask").hasClass("none-box")){
            this.detailsLayout();
        }
    },
    setEvents: function () {
        var self=this;
        $(window).resize(this.setLayout);
        $(".out").on('click',this.out);
        $(".pagination").delegate("li","click",function(e){self.jump(e,self)});
        $("#newFriends").on("click",function(e){self.taskEdit(e,self)});
        $("#taskManagement").delegate("td a.taskEdit","click",function(e){self.taskEdit(e,self)});
        $("#taskManagement").delegate("td a.taskDelete","click",function(e){self.taskDelete(e,self)});
        $("#taskManagement").delegate("td a.lookDetails","click",function(e){self.lookDetails(e,self)});
        $(".detailsMask").on("click",this.detailsMaskCancel);
        $("#search").on("click",function(e){self.requstData(1);});
    },
    taskDelete: function (e, self) {
        var formData=new Object();
        formData.tid=$(e.target).parent().attr("name");
        console.log(formData);
        $.ajax({
            url: 'http://192.168.0.107:2000/pyq/task_del/',
            type: 'GET',
            data: formData,
            success: function (data) {
                console.log(data);
                if(data=="success"){
                    var pageNum=parseInt($("#p1").text());
                    self.requstData(pageNum);
                }
            }
        });
    },
    detailsLayout: function () {
        var bodyW=$(document.body).width();
        var bodyH=$(window).height();
        var detailsH=$(".detailsDiv")[0].offsetHeight;
        var detailsW=$(".detailsDiv")[0].offsetWidth;
        var detailsTop=parseInt((bodyH-detailsH)/2);
        var detailsLeft=parseInt((bodyW-detailsW)/2);
        $(".detailsDiv").css({left:detailsLeft+'px',top:detailsTop+'px',maxHeight:bodyH-100+'px'});
    },
    lookDetails: function (e, self) {
        var formData=new Object();
        formData.tid=$(e.target).parent().attr("name");
        this.requestTaskDetails(formData,function (jsonData) {
            console.log(jsonData);
            if(jsonData.taskTitle!=undefined){
                $(".detailsTitle").append("<span class='detailsText'>标题：</span><span>"+jsonData.taskTitle+"</span>");
            }else{
                $(".detailsTitle").html("<span class='detailsText'>标题：</span>");
            }
            if(jsonData.taskContext!=undefined){
                $(".detailsContext").append("<span class='detailsText'>正文：</span><span>"+jsonData.taskContext+"</span>");
            }else{
                $(".detailsContext").text("");
            }
            if((jsonData.filesPath!=undefined)&&(jsonData.filesPath.length>0)){
                $("#sendImg").empty();
                $.each(jsonData.filesPath,function (i, item) {
                    var html = '<div class="upload_append_list" style="background:url(' + item + ') no-repeat center center;background-size:cover;"></div>';
                    $("#sendImg").append(html);
                });
            }else{
                $("#sendImg").empty();
            }
            if(jsonData.friend_groups!=undefined){
                $("#sendFriends").empty();
                $.each(jsonData.friend_groups,function (i,item) {
                    $("#sendFriends").append('<div class="sendFriends">'+item+'</div>');
                    // if((i+1)%4==0){
                    //     $("#sendFriends").append('<br/>');
                    // }
                });
            }
            if(jsonData.bots!=undefined){
                $("#sendHelpers").empty();
                $.each(jsonData.bots,function (i,item) {
                    $("#sendHelpers").append('<div class="sendHelpers">'+item+'</div>');
                    // if((i+1)%4==0){
                    //     $("#sendHelpers").append('<br/>');
                    // }
                });
            }
            $(".detailsMask").removeClass("none-box");
            self.detailsLayout();
        });
    },
    detailsMaskCancel: function () {
        $(".detailsMask").addClass("none-box");
    },
    taskEdit: function (e,self) {
        var formData=new Object();
        formData.tid=$(e.target).parent().attr("name");
        console.log(formData);//如果formData.taskId是new，表示是新建朋友圈，返回初始化数据
        this.requestTaskDetails(formData,function (jsonData) {
            self.newFriends(jsonData);
        });
        // var data={
        //     taskTitle:"朋友圈一",
        //     taskContext:"啦啦啦啦啦啦啦啦",
        //     filesPath:["http://nfs.gemii.cc/friends/xb8WLtgRg5gana5i..png"],
        //     friendsLabel:[
        //         {name:"1-5月", selected:"checked"},
        //         {name:"产前",selected:""},
        //         {name:"产后",selected:"checked"},
        //         {name:"6-10月",selected:""},
        //         {name:"男宝宝",selected:"checked"},
        //         {name:"女宝宝",selected:"checked"}],
        //     helpersGroup:{
        //         "华东":[{name:"小董1",serial:"01",online:false,selected: ""},
        //             {name:"小董2",serial:"02",online:true,selected: "checked"},
        //             {name:"小董3",serial:"03",online:true,selected: "checked"},
        //             {name:"小董4",serial:"04",online:false,selected: ""},
        //             {name:"小董5",serial:"05",online:true,selected: "checked"},
        //             {name:"小董6",serial:"06",online:true,selected: "checked"}],
        //         "华南":[{name:"小南1",serial:"07",online:true,selected: "checked"},
        //             {name:"小南2",serial:"08",online:true,selected: "checked"},
        //             {name:"小南3",serial:"09",online:true,selected: "checked"},
        //             {name:"小南4",serial:"10",online:false,selected: "checked"},
        //             {name:"小南5",serial:"11",online:true,selected: "checked"},
        //             {name:"小南6",serial:"12",online:true,selected: ""}],
        //         "华西":[{name:"小西1",serial:"13",online:true,selected: "checked"},
        //             {name:"小西2",serial:"14",online:true,selected: "checked"},
        //             {name:"小西3",serial:"15",online:true,selected: ""},
        //             {name:"小西4",serial:"16",online:true,selected: "checked"},
        //             {name:"小西5",serial:"17",online:false,selected: "checked"},
        //             {name:"小西6",serial:"18",online:true,selected: "checked"}],
        //         "华北":[{name:"小北1",serial:"19",online:true,selected: "checked"},
        //             {name:"小北2",serial:"20",online:true,selected: ""},
        //             {name:"小北3",serial:"21",online:true,selected: "checked"},
        //             {name:"小北4",serial:"22",online:false,selected: "checked"},
        //             {name:"小北5",serial:"23",online:false,selected: ""},
        //             {name:"小北6",serial:"24",online:true,selected: ""}],
        //         "华中":[{name:"小钟1",serial:"25",online:true,selected: ""},
        //             {name:"小钟小董2",serial:"26",online:true,selected: ""},
        //             {name:"小钟3",serial:"27",online:true,selected: ""},
        //             {name:"小钟4",serial:"28",online:true,selected: "checked"},
        //             {name:"小钟5",serial:"29",online:true,selected: "checked"},
        //             {name:"小钟6",serial:"30",online:true,selected: ""}]
        //     },
        //     sendTime:{
        //         day:"2017/02/15",
        //         time:"10:00"
        //     }
        // }
        // frdsGrp=data.helpersGroup;
        // self.newFriends(data);
    },
    requestTaskDetails: function (formData,callback) {
        $.ajax({
            url: 'http://192.168.0.107:2000/pyq/task_edit/',
            type: 'GET',
            data: formData,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                var jsonData = {
                    helpersGroup: {},
                    friendsLabel: []
                };
                $.each(data.bots, function (name, value) {
                    jsonData.helpersGroup[name] = [];
                    $.each(value, function (i, item) {
                        console.log(item);
                        var select = "";
                        console.log(data.task);
                        if (data.task != undefined) {
                            $.each(data.task.pyq_account, function (index, s) {
                                if (item.serial == s) {
                                    select = "checked";
                                }
                            });
                        }
                        jsonData.helpersGroup[name].push({
                            name: item.name,
                            serial: item.serial,
                            online: item.sid,
                            selected: select
                        });
                    });
                });
                $.each(data.friend_groups, function (i, item) {
                    var select = "";
                    if (data.task != undefined) {
                        $.each(data.task.pyq_group, function (index, s) {
                            if (item == s) {
                                select = "checked";
                            }
                        });
                    }
                    jsonData.friendsLabel.push({
                        name: item,
                        selected: select
                    });
                });
                if (data.task != undefined) {
                    jsonData.taskTitle = data.task.title;
                    jsonData.taskContext = data.task.pyq_content;
                    jsonData.filesPath = data.task.pyq_imgs;
                    jsonData.numid=data.task.numid;
                    jsonData.friend_groups = data.task.pyq_group;
                    jsonData.bots = data.task.pyq_account;
                }
                console.log(jsonData);
                frdsGrp=jsonData.helpersGroup;
                callback(jsonData);
            }
        });
    },
    initPages: function (id, total, pageNum) {
        //清空列表
        var l=$("#"+id).find("li").length;
        if(l>4){
            for(var i=0; i<l-4;i++){
                if($("#"+id).find("li").length>4){
                    $("#"+id).find("li").eq(2).remove();
                }
            }
        }
        var count=0;
        var start=1;
        if(total>10){
            count=10;
            if(pageNum-5>0){
                start=pageNum-5;
            }
            if(pageNum+4>total){
                count=total;
                start=count-9;
            }else {
                count=start+9
            }

        }else {
            count=total;
        }
        var str="";
        for(var i=start;i<count+1; i++){
            str+="<li><a href='#"+i+"'>"+i+"</a></li> "
        }
        $("#"+id).find("li").eq(1).after(str);
        $("#"+id).find("li").find("a[href='#"+pageNum+"']").parent().addClass("activeli");

        if(total<2){
            if(!$("#"+id).parent().hasClass("none-box")){
                $("#"+id).parent().addClass("none-box");
            }
        }else{
            if($("#"+id).parent().hasClass("none-box")){
                $("#"+id).parent().removeClass("none-box");
            }
        }
    },
    jump: function (e,self) {
        var target=$(e.target).prop('tagName')=="A"?$(e.target).parent():$(e.target);
        var page=target.find("a").attr("href").replace("#","");//点击的页码
        if(page.length>2){
            if(page=="first"){
                self.requstData(1);
            }else if(page=="last"){
                var num=parseInt($("#p2").text());
                self.requstData(num);
            }else if(page=="previous"){
                var num=parseInt($("#p1").text())-1;
                if(num>=1){
                    self.requstData(num);
                }
            }else if(page=="next"){
                var num=parseInt($("#p1").text())+1;
                if(num<=parseInt($("#p2").text())){
                    self.requstData(num);
                }
            }
        }else{
            var num=parseInt(page);
            self.requstData(num);
        }
    },
    out: function () {
        window.location.href="login.html";
    },
    friendsTask:null,
    newFriends: function (data) {
        console.log(data);
        if(this.friendsTask!=null){
            this.friendsTask.render(data);
        }else{
            this.friendsTask=new friendsTask({data:data,app:this});
        }
    }
}

var friendsTask=function (data) {
    console.log(data);
    this.parent=data.app;
    this.initialize(data.data);
}
var nowFile=new Array();
var paths=new Array();
var frdsGrp=new Object();
friendsTask.prototype={
    initialize: function (data) {
        this.render(data);
            this.setEvents();
            this.createEmoji();
            this.connectWebsocket();
    },
    render: function (data) {
        $(".bodyContainer").children("div").addClass("none-box");
        $(".bodyContainer").find(".friendsTask").removeClass("none-box");
        console.log(data);
        if((data!=null)&&(data!=undefined)){
            if((data.numid!=undefined)&&(data.numid!="")){
                $(".friendsTask").attr("name",data.numid);
            }else{
                $(".friendsTask").attr("name","");
            }
            if((data.taskTitle!=undefined)&&(data.taskTitle!="")){
                $("#taskTitle").val(data.taskTitle);
            }else{
                $("#taskTitle").val("");
            }
            if((data.taskContext!=undefined)&&(data.taskContext!=null)){
                $("#taskContext").val(data.taskContext);
                $(".emoji-wysiwyg-editor").text(data.taskContext);
            }else{
                $("#taskContext").val("");
                $(".emoji-wysiwyg-editor").text("");
            }
            if((data.filesPath!=undefined)&&(data.filesPath.length>0)){
                nowFile=data.filesPath;
                paths=data.filesPath;
                $("#previewer").empty();
                $("#alertUplode").empty();
                $.each(paths,function (i, item) {
                    var html = '<div class="upload_append_list" style="background:url(' + item + ') no-repeat center center;background-size:cover;">'+
                        '<a href="javascript:" class="upload_delete" title="删除"></a><br />'+
                        '</div>';
                    $("#previewer").append(html);
                });
            }else{
                $("#alertUplode").empty();
                $("#previewer").empty();
            }
            if(data.friendsLabel.length>0){
                $("#friendsLabel").empty();
                $.each(data.friendsLabel,function (i, item) {
                    $("#friendsLabel").append('<input className="checkInput" type="checkbox" name="friendsGroup" value='+item.name+' '+item.selected+' /><span>'+item.name+'</span>');
                    if((i+1)%3==0){
                        $("#friendsLabel").append("<br/>");
                    }
                });
            }
            if(data.helpersGroup!=null){
                $("#helperGroup").empty();
                $("#selectList").empty();
               $.each(data.helpersGroup,function (key, item) {
                   $("#helperGroup").append('<option value='+key+' name='+key+'>'+key+'</option>');
                   $.each(item,function (i,label) {
                       if(label.selected!=""){
                           $("#selectList").append('<div class="selectHelper" id='+label.serial+' >'+label.name+'</div>');
                       }
                   });

               });
                this.selectHelperGroup();
            }
        }
    },
    setEvents: function () {
        var self=this;
        $("#addImg").on("click",this.openFile);
        $("#fileB").on("change",function(e){
            self.fileSelect(e,self);
        });
        $("#previewer").delegate("a.upload_delete","click", this.fileDelete);
        $("#immediatelySend").on("click",function(e){self.immediatelySend(e,self,'now');});
        $("#laterSend").on("click",function(e){self.immediatelySend(e,self,'laterSend');});
        $("#taskCancel").on("click",this.cancel);
        $("#helperGroup").on("change",this.selectHelperGroup);
        $("#selectAll").on("click",this.selectAll);
        $("#helperform").delegate("input","mouseup",this.toggleSelect);
        // $("#uplode").on("click",this.uplodeImg);
    },
    createEmoji: function () {
        window.emojiPicker = new EmojiPicker({
            emojiable_selector: '[data-emojiable=true]',
            assetsPath: 'lib/img/',
            popupButtonClasses: 'fa fa-smile-o'
        });
        window.emojiPicker.discover();
        $(".emoji-menu-tabs").find(".icon-smile").click();
        $(".emoji-menu-tabs").remove();
    },
    connectWebsocket: function () {
        // ab.connect("ws://192.168.0.107:12306",
        //     function (session) {
        //         console.log("Connected!");
        //         console.log(session);
        //         session.subscribe("webadmin", onEvent);
        //     },
        //     function (code, reason, detail) {
        //         console.log("WAMP session is gone");
        //     },
        //     {
        //         'maxRetries': 10,
        //         'retryDelay': 2000
        //     }
        // );
        // function onEvent(data) {
        //     console.log(data);
        // }
    },
    immediatelySend: function (e,self,time) {
        console.log($("#taskContext").val().toString());
        var friendsLabel=$("#friendsLabel").find("input:checked");
        var labels=new Array();
        $.each(friendsLabel,function (i, item) {
            labels.push(item.value);
        });
        if(time!="now"){
            time=$("#day").find("option:selected").val()+'-'+$("#time").find("option:selected").val();
        }
        var selectList=$("#selectList").find("div");
        var accList=new Array();
        $.each(selectList,function (i, item) {
            accList.push($(item).attr("id"));
        });
        var formData={
            "title":$("#taskTitle").val(),
            "pyq_content":$("#taskContext").val(),
            "pyq_mp4":"",
            "run_time":time,
            "pyq_imgs":paths,
            "pyq_group":labels,
            "pyq_account":accList,
            "numid":$(".friendsTask").attr("name")
        };
        console.log(formData);
        if((formData.pyq_content=="")&&(paths.length==0)){
            alert("发送的图片和内容不能都未空！");
        }else if(formData.pyq_account.length<=0){
            alert("覆盖的小助手不能为空");
        }else{
            $.post('http://192.168.0.107:2000/pyq/task_post/',
                JSON.stringify(formData),
                function (data) {
                    console.log(data);
                    var pageNum=parseInt($("#p1").text());
                    console.log(self.parent);
                    self.parent.requstData(pageNum);
                });
            self.cancel();
        }
    },
    openFile: function () {
        $("#fileB").click();
    },
    selectAll: function () {
        var checkbox=$("#helperform").find("input");
        $.each(checkbox,function (i,item) {
            if(!item.checked){
                $("#selectList").append('<div class="selectHelper" id='+item.id+' >'+item.value+'</div>');
            }
        });
        checkbox.prop("checked","checked");
    },
    toggleSelect: function () {
        if(!$(this).prop("checked")){
            $(this).prop("checked","");
            $("#selectList").append('<div class="selectHelper" id='+this.id+' >'+this.value+'</div>');
        }else{
            $(this).prop("checked","checked");
            $("#selectList").find("div[id="+this.id+"]").remove();
        }
    },
    fileSelect: function (e,self) {
        var files = e.target.files || e.dataTransfer.files; //获取图片资源
        var filterFiles=self.filter(files);
        $("#alertUplode").empty();
        $("#alertUplode").append("<span class='fisrtSpan'>正在上传图片请稍后...</span>");
        self.uplodeImg(filterFiles);
        nowFile=nowFile.concat(filterFiles);
        // files=nowFile;
        // var html='';
        // $("#previewer").empty();
        // $.each(files,function (i, file) {
        //     // 只选择图片文件
        //     if (!file.type.match('image.*')) {
        //         return false;
        //     }
        //     var reader = new FileReader();
        //     reader.readAsDataURL(file); // 读取文件
        //     // 渲染文件
        //     reader.onload = function(arg) {
        //         html = '<div id="uploadList_'+ i +'" class="upload_append_list" style="background:url(' + arg.target.result + ') no-repeat center center;background-size:cover;">'+
        //             '<a href="javascript:" class="upload_delete" title="删除" data-index="'+ i +'"></a><br />'+
        //             '</div>';
        //         $("#previewer").append(html);
        //     }
        // });
    },
    fileDelete: function () {
        var fileDelete=nowFile[$(this).parent().index()];
        var arrFile = [];
        for (var i = 0, file; file = nowFile[i]; i++) {
            if (file != fileDelete) {
                arrFile.push(file);
            } else {
                paths.splice(i,1);
                console.log(paths);
                $(this).parent().remove();
            }
        }
        nowFile = arrFile;
    },
    filter: function(files) {
        var arrFiles = [];
        for (var i = 0, file; file = files[i]; i++) {
            if (file.type.indexOf("image") == 0) {
                if (file.size >= 1048576){
                    alert('您这张图片('+file.name+')大小过大，应小于10M');
                } else {
                    arrFiles.push(file);
                }
            } else {
                alert('您选择的不是图片。');
            }
        }
        if(arrFiles.length+nowFile.length>10){
            var n=9-nowFile.length;
            arrFiles.splice(n,arrFiles.length-n);
        }
        return arrFiles;
    },
    uplodeImg:function (files) {
        console.log(files);
        if(files.length>0){
            var flag=0;
            var errorC=0;
            $.each(files,function (i, file) {
                var formData = new FormData();
                formData.append("file", files[i]);
                formData.append("dir","friends");
                $.ajax({
                    url: 'http://helper.gemii.cc/GroupManage/upload/uploadFile',
                    type: 'POST',
                    cache: false,
                    aysnc: true,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        paths.push(data.data);
                        flag++;
                        if(flag==files.length){
                            $("#alertUplode").empty();
                        }
                        console.log(paths);
                        var html = '<div class="upload_append_list" style="background:url(' + data.data + ') no-repeat center center;background-size:cover;">'+
                            '<a href="javascript:" class="upload_delete" title="删除"></a><br />'+
                            '</div>';
                        $("#previewer").append(html);
                        if($("#previewer").children("div").length%3==0){
                            $("#previewer").append("<br/>");
                        }
                    },
                    error: function (data) {
                        errorC++;
                        if(errorC+flag==files.length){
                            $("#alertUplode").find(".fisrtSpan").remove();
                            setTimeout(function () {
                                $("#alertUplode").empty();
                            },10000);
                        }
                        $("#alertUplode").append("<br/><span>图片"+file.name+"&nbsp;&nbsp;&nbsp;上传失败</span>");
                    }
                });
            });
        }
    },
    cancel: function () {
        $(".bodyContainer").children("div").addClass("none-box");
        $(".bodyContainer").find(".mainDiv").removeClass("none-box");
    },
    selectHelperGroup: function () {
        $("#helperform").empty();
        var key=$("#helperGroup").find("option:selected").text();
        var helpers=frdsGrp[key];
        console.log(helpers);
        $.each(helpers,function (i, item) {
            if(item.online){
                $("#helperform").append('<input className="checkInput" type="checkbox" name="helperGroup" id='+item.serial+' value='+item.name+' '+item.selected+' /><span>'+item.name+'</span><div class="online"></div>');
            }else{
                $("#helperform").append('<input className="checkInput" type="checkbox" name="helperGroup" id='+item.serial+' value='+item.name+' '+item.selected+' /><span>'+item.name+'</span><div class="online offline"></div>');
            }
            if((i+1)%4==0){
                $("#helperform").append("<br/>");
            }
        });
    }
}
